﻿using System;
using FlightSearch.Driver.Classes;
using NUnit.Framework;

namespace FlightSearch.Driver.Tests
{
    [TestFixture]
    public class FlightSearchTest
    {
        private IFlight _iFlight;
        [TestFixtureSetUp]
        public void Setup()
        {
            _iFlight = IocUtil.Resolve<IFlight>();
        }

        [Test]
        public void FlightSearch_Result()
        {
            var result = _iFlight.Search(new FlightSearchCriteria
            {
                Adult = 2,
                DepartureDate = new DateTime(2013, 10, 10),
                Child = 0,
                FromCity = "AYT",
                ToCity = "IST",
                ReturnDate = null
            });

            Assert.AreNotEqual(0, result.Count);
        }
    }
}
